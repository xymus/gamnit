check: unit-tests check-linux check-android

unit-tests:
	make check -C examples/template/
	make check -C examples/triangle/
	make check -C examples/fonts_showcase/

examples-linux:
	make -C examples/template/
	make -C examples/triangle/
	make -C examples/fonts_showcase/

examples-android:
	make android -C examples/template/
	make android -C examples/triangle/
	make android -C examples/fonts_showcase/

clean:
	make clean -C examples/template/
	make clean -C examples/triangle/
	make clean -C examples/fonts_showcase/
