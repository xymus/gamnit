# This file is part of Gamnit ( http://gamnit.org ).
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Services for scripts
module tools_common

import prompt

redef class Text

	# Execute shell command `self`, show it to user if it has an `effect`
	fun sh(effect: nullable Bool): String
	do
		effect = effect or else false

		if effect then
			print "+ " + self
			if args.has("-i") then
				var res = prompt("  Execute the previous command? [Y/n] ")
				if res == null or not (res == "" or res.to_lower == "y" or res.to_lower == "yes") then
					exit 1
				end
			end
		end

		if effect and args.has("-d") then
			return ""
		end

		var proc = new ProcessReader("sh", "-c", self)
		proc.wait
		var res = proc.read_all.chomp
		proc.close

		if proc.status != 0 and not args.has("-f") then
			print_error "Command failed: {self}"
			exit 1
		end

		return res
	end
end
