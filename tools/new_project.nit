# This file is part of Gamnit ( http://gamnit.org ).
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Script to create a new Gamnit project from the template project
#
# Usage: `nit gamnit::new_project my_project_name`
module new_project

import tools_common

private fun print_help do print """
usage: nit gamnit::setup <project_name> [options]

options:
  -i  Interactive, confirm each command.
  -f  Force, ignore failed commands and keep going.
  -d  Dry run, print out the commands but don't execute them.
  -h  Show this help message.
"""

if args.has("-h") then
	print_help
	exit 0
end

var project_name = null
for arg in args do
	if not arg.has_prefix("-") then
		project_name = arg
		break
	end
end

if project_name == null then
	print "No project_name specified"
	print_help
	exit 1
	abort
end

var project_name_valid = not project_name.is_empty and project_name.chars.first.is_lower
for c in project_name do
	if not c.is_alphanumeric or c == '_' then
		project_name_valid = false
		break
	end
end
if not project_name_valid then
	print "Project name '{project_name}' invalid, it must start with a lowercase letter and contain only letters, digits and underscores."
	exit 1
end

var gamnit_dir = "nitls -pP gamnit".sh
if gamnit_dir.is_empty then
	print "gamnit not found"
	exit 1
end

var template_dir = gamnit_dir / "examples/template"

if project_name.file_exists then
	print "The project folder at `{project_name}` exists, refusing to overwrite."
	exit 1
end

"cp -r '{template_dir}' {project_name}".sh(true)
"mv {project_name}/src/template.nit {project_name}/src/{project_name}.nit".sh(true)
"cd {project_name}; find -type f -exec sed -i -e s/template/{project_name}/g \{\} \\;".sh(true)

print """

Project '{{{project_name}}}' is ready!

You can now `cd {{{project_name}}}', compile it with `make` and launch it with `bin/{{{project_name}}}`."""
