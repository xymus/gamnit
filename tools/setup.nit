# This file is part of Gamnit ( http://gamnit.org ).
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Script to install the required development packages automatically
#
# Usage: `nit gamnit::setup [options]`
module setup

private import tools_common

if args.has("-h") then
	print """
usage: nit gamnit::setup [options]

options:
  -i  Interactive, confirm each command.
  -f  Force, ignore failed commands and keep going.
  -d  Dry run, print out the commands but don't execute them.
  -h  Show this help message.
  linux | darwin | mingw  Simulate for a specific platform.
"""
	exit 0
end

print """
To compile gamnit apps, you will need development packages for
OpenGL ES 2.0, SDL2, SDL2_image and SDL2_mixer, and generally
Inkscape to compile some examples.

We'll now try to install them automatically...
"""

# General to Nit: build-essential ccache libgc-dev graphviz libunwind-dev pkg-config
var debian_install = "sudo apt-get install libgles2-mesa-dev libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev inkscape unzip"

var uname = "uname".sh
print "Detected platform: {uname}\n"

if uname.has("Linux") or args.has("linux") then

	var id = "cat /etc/os-release | grep ^ID= | sed s/ID=//".sh
	var has_apt_get = "which apt-get".sh.length > 0
	if id == "debian" or id == "ubuntu" then
		debian_install.sh(true)
	else if has_apt_get then
		print "Your Linux distribution ({id}) is not fully supported by this tool, but the following may work."
		debian_install.sh(true)
	else
		print "Your Linux distribution ({id}) is not supported by this tool."
		print "You will need to manually install the required packages, sorry."
	end

	# Android?
	var res = prompt("Do you want to create Android apps? [Y/n] ")
	if res != null and (res == "" or res.to_lower == "y" or res.to_lower == "yes") then
		print "Fetch and extract SDK tools..."
		"mkdir -p ~/Android/Sdk".sh(true)
		"cd ~/Android/Sdk".sh(true)
		"wget https://dl.google.com/android/repository/sdk-tools-linux-3859397.zip".sh(true)
		"unzip sdk-tools-linux-3859397.zip".sh(true)

		print "Update tools..."
		"tools/bin/sdkmanager --update".sh(true)

		print "Accept the licenses..."
		"tools/bin/sdkmanager --licenses".sh(true)

		print "Install the basic build tools..."
		"tools/bin/sdkmanager 'build-tools;27.0.0' ndk-bundle".sh(true)

		print "Install Java 8 JDK..."
		"sudo apt-get install openjdk-8-jdk".sh(true)

		print "Set the environment variable ANDROID_HOME to the SDK installation directory, ~/Android/Sdk/."
		"echo 'export ANDROID_HOME=~/Android/Sdk/' >> ~/.bashrc".sh(true)
	end


else if uname.has("Darwin") or args.has("darwin")  then
	print "macOS is not (yet) supported as a gamnit target, however it can create apps for iOS.\n"

	# These are already general to Nit
	"brew install ccache bdw-gc libunwind-headers pkgconfig graphviz ios-sim".sh(true)

else if uname.has("MINGW") or args.has("mingw") then
	# General to Nit: make git ccache pkg-config libgc-devel diffutils mingw-w64-x86_64-gc mingw-w64-x86_64-libunwind mingw-w64-x86_64-pcre mingw-w64-x86_64-libsystre mingw-w64-x86_64-dlfcn
	"pacman -S mingw-w64-x86_64-angleproject-git mingw-w64-x86_64-SDL2 mingw-w64-x86_64-SDL2_image mingw-w64-x86_64-SDL2_mixer".sh(true)

else
	print "Your platform ({uname}) is not supported by this tool."
end
